using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ArrayEvent
{
    public class CustomArray<T> : IEnumerable<T>
    {

        public delegate void ArrayHandler(object sender, ArrayEventArgs<T> e);

 
        public event ArrayHandler OnChangeElement;

        public void ChangeElement(int index, string message, T value)
        {
            int temp = index - First;
            if (Array[temp] == null || !Array[temp].Equals(value))
            {
                OnChangeElement?.Invoke(this, new ArrayEventArgs<T>(index, "message", value));
            }
        }

        public event ArrayHandler OnChangeEqualElement;

        public void ChangeEqualElement(int index, string message, T value)
        {
            int temp = index - First;
            if (!(temp).Equals(value)|| !Array[temp].Equals(value))
            {
                OnChangeEqualElement?.Invoke(this, new ArrayEventArgs<T>(index, "message", value));
            }
        }

    public int First
        {
            get;
            private set;
        }

        public int Last
        {
            get
            {
                return First + Length - 1;
            }
        }

        public int Length
        {
            get
            {
                return Array.Length;
            }
            private set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Value less than 0");
                }
            }
        }

        public T[] Array
        {
            get;
        }
      
        public CustomArray(int first, int length)
        {
            First = first;
            Length = length;
            Array = new T[length];
        }

        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list == null)
            {
                throw new NullReferenceException("List is null");
            }

            if (list.Count() <= 0)
            {
                throw new ArgumentException("Count less than 0");
            }

            First = first;
            Array = list.ToArray();
            Length = Array.Length;

        }

        public CustomArray(int first, params T[] list)
        {
            if (list == null)
            {
                throw new ArgumentNullException("List is null");
            }

            if (list.Count() <= 0)
            {
                throw new ArgumentException("List without elements");
            }

            First = first;
            Array = list;
            Length = list.Length;
        }

        public T this[int index]
        {
            get
            {
                if (index < First || index > Last)
                {
                    throw new ArgumentException("Index out of array rang");
                }

                return Array[index - First];
            }
            set
            {
                if (index < First || index > Last)
                {
                    throw new ArgumentException("Index out of array rang");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("Value passed in indexer is null");
                }
                ChangeElement(index, "message", value);
                ChangeEqualElement(index, "message", value);

                Array[index - First] = value;
            }
        }
     
        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)Array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Array.GetEnumerator();
        }
    }
}
